// déclaration des variables
let poids = "";
let taille = "";
let IMC = "";
let interpretation = "";

$("#btnCalcul").click(                              // click sur le bouton "btnCalcul"
    function () {

        poids = $("#zonePoids").val();              // prend la valeur de la zone de saisie du poids
        taille = $("#zoneTaille").val();            // prend la valeur de la zone de saisie de la taille

        // vérification saisie
        if (isNaN(poids) || isNaN(taille)) {        // si un champ contient autre chose qu'un chiffre
            alert("Erreur de saisie")               // message d'erreur
        } else {

            // règles à respecter pour les varaibles
            poids = poids.replace(",", ".");            // remplacement des "," par des "."
            poids = Number(poids);                      // converti en nombre

            taille = taille.replace(",", ".");          // remplacement des "," par des "."
            taille = Number(taille);                    // converti en nombre

            // affiche le résultat de la fonction CalculerIMC
            IMC = CalculerIMC(poids, taille);                                   // calcul de l'IMC
            interpretation = InterpreterIMC(IMC);                               // interpretation de l'IMC                            
            $("#resultat").html(IMC + " (" + interpretation + ")");             // affiche le résultat et l'interprétation
        }
    }
);

function CalculerIMC(prmPoids, prmTaille) {         // déclaration de la fonction Calculer IMC
    let res = prmPoids / (prmTaille * prmTaille);   // calcul de l'IMC
    return res.toFixed(1);                          // retourne le résultat
}

function InterpreterIMC(prmIMC) {                   // déclaration de la fonction InterpreterIMC
    let inter = "";                                 // variable qui prend le message

    switch (true) {                                 // selon les valeurs de l'IMC
        case (prmIMC < 16.5):                       // inférieur à 16.5
            inter = "dénutrition";                  // le message indiquera dénutrition
            break;
        case (prmIMC >= 16.5 && prmIMC < 18.5):     // compris entre 16.5 et 18.5
            inter = "maigreur";                     // le message indiquera maigreur
            break;
        case (prmIMC >= 18.5 && prmIMC < 25):       // compris entre 18.5 et 25
            inter = "corpulence normale";           // le message indiquera corpulence normale
            break;
        case (prmIMC >= 25 && prmIMC < 30):         // compris entre 25 et 30
            inter = "surpoids";                     // le message indiquera surpoids
            break;
        case (prmIMC >= 30 && prmIMC < 35):         // compris entre 30 et 35
            inter = "obésité modérée";              // le message indiquera obésité modérée
            break;
        case (prmIMC >= 35 && prmIMC < 40):         // compris entre 35 et 40
            inter = "obésité sévère";               // le message indiquera obésité sévère
            break;
        case (prmIMC >= 40):                        // supérieur à 40
            inter = "obésité morbide";              // le message indiquera obésité morbide
            break;
        default:                                    // par défaut
            inter = "erreur";                       // message d'erreur
    }
    return inter;                                   // retourne l'interprétation
}