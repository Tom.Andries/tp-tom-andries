$(document).ready(function () {
    $("#resultat").html(AfficherIMC());                 // affiche l'IMC de base

    $("#curseurPoids").on('input', function () {        // lorsqu'on bouge le curseur du poids
        AfficherIMC();                                  // affiche le calcul de l'IMC et de l'interprétation
    });

    $("#curseurTaille").on('input', function () {       // lorsqu'on bouge le curseur de la taille
        AfficherIMC();                                  // affiche le calcul de l'IMC et de l'interprétation
    });

    function AfficherIMC() {                            // fonction d'affichage de l'IMC
        // déclaration des variables
        let poids = 0;
        let taille = 0;
        let IMC = 0;
        let interpretation = "";

        poids = $("#curseurPoids").val();               // prend la valeur de la zone de saisie du poids
        $("#textPoids").html(poids);                    // affiche la valeur du curseur

        taille = $("#curseurTaille").val();             // prend la valeur de la zone de saisie de la taille
        $("#textTaille").html(taille);                  // affiche la valeur du curseur

        // conversions des valeurs
        poids = Number(poids);                          // converti la valeur en nombre
        taille = Number(taille) / 100;                  // converti les cm en m

        IMC = CalculerIMC(poids, taille);               // appel de la fonction qui calcule l'IMC
        interpretation = InterpreterIMC(IMC);           // appel de la fonction qui interprète l'IMC
        balance = AfficherBalance(IMC);                 // appel de la fonction qui affiche l'IMC sur la balance

        $("#resultat").html(IMC + interpretation);      // affiche l'IMC dans la balise #resultat
        $("#aiguille").css("left", balance + "px");     // déplace l'aiguille sur la balance
    }

    function CalculerIMC(prmPoids, prmTaille) {         // déclaration de la fonction Calculer IMC
        let res = 0;
        res = prmPoids / (prmTaille * prmTaille);       // calcul de l'IMC
        return res.toFixed(1);                          // retourne le résultat au dixième près
    }

    function InterpreterIMC(prmIMC) {                   // déclaration de la fonction InterpreterIMC
        let inter = "";                                 // variable qui prend le message

        switch (true) {                                 // selon les valeurs de l'IMC
            case (prmIMC < 16.5):                       // inférieur à 16.5
                inter = " (dénutrition)";               // le message indiquera dénutrition
                break;
            case (prmIMC >= 16.5 && prmIMC < 18.5):     // compris entre 16.5 et 18.5
                inter = " (maigreur)";                  // le message indiquera maigreur
                break;
            case (prmIMC >= 18.5 && prmIMC < 25):       // compris entre 18.5 et 25
                inter = " (corpulence normale)";        // le message indiquera corpulence normale
                break;
            case (prmIMC >= 25 && prmIMC < 30):         // compris entre 25 et 30
                inter = " (surpoids)";                  // le message indiquera surpoids
                break;
            case (prmIMC >= 30 && prmIMC < 35):         // compris entre 30 et 35
                inter = " (obésité modérée)";           // le message indiquera obésité modérée
                break;
            case (prmIMC >= 35 && prmIMC < 40):         // compris entre 35 et 40
                inter = " (obésité sévère)";            // le message indiquera obésité sévère
                break;
            case (prmIMC >= 40):                        // supérieur à 40
                inter = " (obésité morbide)";            // le message indiquera obésité morbide
                break;
            default:                                    // par défaut
                inter = " (erreur)";                    // message d'erreur
        }
        return inter;                                   // retourne l'interprétation
    }

    function AfficherBalance(prmIMC) {                  // déclaration de la fonction AfficherBalance
        let deplacement = 0;                            // déplacement en pixels

        if ((prmIMC >= 10) && (prmIMC <= 45)) {         // si l'IMC est compris entre 10 et 45
            deplacement = (60 / 7) * prmIMC - (600 / 7);    // calcul du déplacement
            return deplacement;                         // retourne le déplacement
        }
    }
});