$(document).ready(function () {
    $("#resultat").html(AfficherIMC());                 // affiche l'IMC de base

    $("#curseurPoids").on('input', function () {        // lorsqu'on bouge le curseur du poids
        AfficherIMC();                                  // affiche le calcul de l'IMC et de l'interprétation
    });

    $("#curseurTaille").on('input', function () {       // lorsqu'on bouge le curseur de la taille
        AfficherIMC();                                  // affiche le calcul de l'IMC et de l'interprétation
    });

    $("input[name=sexe]").on('change', function () {    // lorsqu'on choisi un sexe
        AfficherIMC();
    });

    function AfficherIMC() {                            // fonction d'affichage de l'IMC
        // variable qui contient l'IMC
        let IMC = 0;
        // calcul
        IMC = CalculerIMC();                            // Calcul de l'IMC
        // affichages
        InterpreterIMC(IMC);                            // affiche l'IMC et son interprétation
        AfficherBalance(IMC);                           // affiche l'IMC sur la balance
        AfficherSilhouette(IMC);                        // affiche la silhouette de l'IMC
    }

    function CalculerIMC() {                            // déclaration de la fonction Calculer IMC
        let res = 0;                                    // résultat du calcul de l'IMC
        let poids = 0;                                  // poids en kg
        let taille = 0;                                 // taille en cm

        poids = $("#curseurPoids").val();               // prend la valeur de la zone de saisie du poids
        $("#textPoids").html(poids);                    // affiche la valeur du curseur

        taille = $("#curseurTaille").val();             // prend la valeur de la zone de saisie de la taille
        $("#textTaille").html(taille);                  // affiche la valeur du curseur

        // conversions des valeurs
        poids = Number(poids);                          // converti la valeur en nombre
        taille = Number(taille) / 100;                  // converti les cm en m

        res = poids / (taille * taille);       // calcul de l'IMC
        return res.toFixed(1);                          // retourne le résultat au dixième près
    }

    function InterpreterIMC(prmIMC) {                   // déclaration de la fonction InterpreterIMC
        let interpretation = prmIMC + "";               // contient l'IMC et le message

        switch (true) {                                 // selon les valeurs de l'IMC
            case (prmIMC < 16.5):                       // inférieur à 16.5
                interpretation += " (dénutrition)";             // le message indiquera dénutrition
                break;
            case (prmIMC >= 16.5 && prmIMC < 18.5):     // compris entre 16.5 et 18.5
                interpretation += " (maigreur)";                // le message indiquera maigreur
                break;
            case (prmIMC >= 18.5 && prmIMC < 25):       // compris entre 18.5 et 25
                interpretation += " (corpulence normale)";      // le message indiquera corpulence normale
                break;
            case (prmIMC >= 25 && prmIMC < 30):         // compris entre 25 et 30
                interpretation += " (surpoids)";                // le message indiquera surpoids
                break;
            case (prmIMC >= 30 && prmIMC < 35):         // compris entre 30 et 35
                interpretation = " (obésité modérée)";          // le message indiquera obésité modérée
                break;
            case (prmIMC >= 35 && prmIMC < 40):         // compris entre 35 et 40
                interpretation += " (obésité sévère)";          // le message indiquera obésité sévère
                break;
            case (prmIMC >= 40):                        // supérieur à 40
                interpretation += " (obésité morbide)";         // le message indiquera obésité morbide
                break;
            default:                                    // par défaut
                interpretation += " (erreur)";                  // message d'erreur
        }
        $("#resultat").html(interpretation);      // affiche l'IMC dans la balise #resultat
    }

    function AfficherBalance(prmIMC) {                          // déclaration de la fonction AfficherBalance
        let deplacement = 0;                                    // déplacement en pixels

        if ((prmIMC >= 10) && (prmIMC <= 45)) {                 // si l'IMC est compris entre 10 et 45
            deplacement = (60 / 7) * prmIMC - (600 / 7);        // calcul du déplacement
            $("#aiguille").css("left", deplacement + "px");     // déplace l'aiguille sur la balance
        }
    }

    function AfficherSilhouette(prmIMC) {
        let decalage = 0;                                       // valeur de retour du décalage
        let sexe = $("input[name=sexe]:checked").val();         // sexe de la personne
        const image = 105;                                      // largeur silhouette en pixels

        if (sexe == "homme") {
            $("#silhouette").css("background-image", "url(css/img/IMC-homme.jpg");
        } 
        if (sexe == "femme") {
            $("#silhouette").css("background-image", "url(css/img/IMC-femme.jpg");
        }

        switch (true) {                                         // selon les valeurs de l'IMC

            case (prmIMC >= 16.5 && prmIMC < 18.5):     // compris entre 16.5 et 18.5
                decalage = 5 * image;               // décalage en pixels
                break;
            case (prmIMC >= 18.5 && prmIMC < 25):       // compris entre 18.5 et 25
                decalage = 4 * image;               // décalage en pixels
                break;
            case (prmIMC >= 25 && prmIMC < 30):         // compris entre 25 et 30
                decalage = 3 * image;               // décalage en pixels
                break;
            case (prmIMC >= 30 && prmIMC < 35):         // compris entre 30 et 35
                decalage = 2 * image;               // décalage en pixels
                break;
            case (prmIMC >= 35):                        // supérieur ou égal à 35
                decalage = image;                   // décalage en pixels
                break;
            default:                                    // valeur par défaut
                decalage = 0;
        }
        // déplacer la silhouette
        $("#silhouette").css("background-position", decalage);
    }
});