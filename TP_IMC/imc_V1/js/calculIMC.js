// déclaration des variables
let poids = "";
let taille = "";

$("#btnCalcul").click(
    function () {

        poids = $("#zonePoids").val();              // prend la valeur de la zone de saisie du poids
        taille = $("#zoneTaille").val();            // prend la valeur de la zone de saisie de la taille

        // vérification saisie
        if (isNaN(poids) || isNaN(taille)) {        // si un champ contient autre chose qu'un chiffre
            alert("Erreur de saisie")               // message d'erreur
        } else {

            // règles à respecter pour les varaibles
            poids = poids.replace(",", ".");            // remplacement des "," par des "."
            poids = Number(poids);                      // converti en nombre

            taille = taille.replace(",", ".");          // remplacement des "," par des "."
            taille = Number(taille);                    // converti en nombre

            // affiche le résultat de la fonction CalculerIMC
            $("#resultat").replaceWith(CalculerIMC(poids, taille));
        }
    }
);

function CalculerIMC(prmPoids, prmTaille) {         // déclaration de la fonction Calculer IMC
    let IMC = prmPoids / (prmTaille * prmTaille);   // calcul de l'IMC
    return IMC.toFixed(1);                          // retourne le résultat
}