$(document).ready(function () {                                             // quand la DOM est chargé
    $("#total").html(calculTotal());                                        // affiche le total
    $("#BtnAjouter").click(function () {                                    // click sur le bouton ajouter
        let longueur = $("tbody tr").length + 1;                            // variable qui prend la longueur de la liste
        $("tbody").append("<tr><td>" + longueur + "</td></tr>");            // ajout d'une ligne et de sa valeur
        $("#total").html(calculTotal());                                    // calcul du total grâce à la fonction
    })

    $("#BtnSupprimer").click(function () {                                  // click sur le bouton supprimmer
        $("tbody tr:last-child").remove();                                  // supprime le dernier élément
        $("#total").html(calculTotal());                                    // calcul de total grâce à la fonction
    })

    function calculTotal() {                                                // délcaration de la fonction calculTotal
        let somme = 0;                                                      // variable qui contient le total
        $("tbody td").each(function (index) {                               // pour chaque élément "tbody tr"
            somme += parseInt($(this).html());                              // ajoute la valeur de l'élément
        })
        return somme;                                                       // retourne la somme
    }
});