let nbrTerme = prompt("Saisissez le nombre de valeurs :") - 2;      // saisi du nombre de termes
alert("Suite de Fibonacci :\n" + Fibo(nbrTerme));                   // affichage de la suite

function Fibo (prmNbrTerme) {                                       // fonction qui calcul al suite de Fibonacci
    // déclaration des varaibles
    let terme1 = 0;
    let terme2 = 1;
    let res = 0;                                // résultat de l'addition
    let chaine = terme1 + "-" + terme2;         // chaine de caractère de la suite

    for (let i = 0; i < prmNbrTerme; i++) {     // boucle pour le nombre de valeurs saisis
        res = terme1 + terme2;                  // addition
        chaine = chaine + "-" + res;            // ajout du résultat à la chaîne
        terme1 = terme2;                        // terme1 prend la valeur de terme2
        terme2 = res;                           // terme2 prend la valeur du résultat
    }
    return chaine;                              // retour la chaine des termes
}