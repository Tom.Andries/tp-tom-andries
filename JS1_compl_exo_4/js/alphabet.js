alert(Alphabet());                                      // affichage de la fonction

function Alphabet () {                                  // fonction Alphabet qui ajoute l'alphabet à une chaîne de caractère
    let chaine = "";                                    // chaine de caractère qui contiendra l'alphabet
    let cara = 65;                                      // code de "A"
    let lettre = "";                                    // variable qui contient une lettre
    for (let i =0; i <= 25; i++) {                      // boucle allant de A à Z
        lettre = String.fromCharCode(cara);             // ajout de la lettre dans la variable
        cara++;                                         // incrémentation de cara
        chaine += lettre + " ";                         // ajout de la lettre dans la chaîne
    }
    return chaine;                                      // retourne la chaîne
}